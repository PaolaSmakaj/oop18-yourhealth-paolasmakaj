### Guida utente ###

Per poter accedere al lato admin è necessario l’inserimento di una password (“pas”, decisa di default). Per utilizzare, invece, i bottoni “Dottore” e “Paziente” è fondamentale registrarli prima dopo aver acceduto come amministratore, cosicché questi abbiano un tesserino e un codice fiscale con cui identificarsi.

Paola Smakaj
