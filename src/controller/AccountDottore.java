package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import model.Dottore;
import model.DottoreImpl;
import model.Prestazione;
import model.PrestazioneImpl;
import util.Enums;
import util.Enums.Sangue;

public class AccountDottore extends Account{

	public static ArrayList<Prestazione> listaPrestazioniDottore(int tesserino) {
		try {
			Connection con = Database.getConnection();
			PreparedStatement statement = con
					.prepareStatement("SELECT * FROM prestazioni WHERE Tesserino=?");
			statement.setInt(1, tesserino);;

			ResultSet result = statement.executeQuery();

			ArrayList<Prestazione> array = new ArrayList<Prestazione>();
			while (result.next()) {

				Prestazione Pr = new PrestazioneImpl.Builder().paziente(result.getString("CfPaziente")).dottore(result.getInt("Tesserino"))
						.tipoprestazione(Enums.TipoPrestazione.getFromString(result.getString("Tipo")))
						.data(LocalDate.parse(result.getString("Data"), DateTimeFormatter.ofPattern("yyyy-MM-dd")))
						.ora(LocalTime.parse(result.getString("Ora"), DateTimeFormatter.ofPattern("HH:mm:ss")))
						.stato(Enums.Stato.getFromString(result.getString("Stato"))).macchinario(result.getString("Macchinario"))
						.ambulatorio(result.getString("Ambulatorio")).build();

				array.add(Pr);
			}
			con.close();
			statement.close();
			return array;

		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}
	
	public static Dottore getDottoreFromDB(int tesserino) {
		Dottore D = null;
		try {
			Connection con = Database.getConnection();
			PreparedStatement statement = con
					.prepareStatement("SELECT * FROM dottori WHERE Tesserino=?");
			statement.setInt(1, tesserino);

			ResultSet result = statement.executeQuery();
			result.next();
			D = new DottoreImpl(result.getString("Nome"), result.getString("Cognome"),
					Enums.Sesso.getFromString(result.getString("Sesso")), result.getString("LuogoNascita"),
					LocalDate.parse(result.getString("DataNascita"), DateTimeFormatter.ofPattern("yyyy-MM-dd")),
					result.getInt("Tesserino"), Enums.Ruolo.getFromString(result.getString("Ruolo")),
					LocalTime.parse(result.getString("OrarioInizio"), DateTimeFormatter.ofPattern("HH:mm:ss")),
					LocalTime.parse(result.getString("OrarioFine"), DateTimeFormatter.ofPattern("HH:mm:ss")));

			statement.close();
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return D;
	}

	public static boolean checkEsistenzaTesserino (int tesserino) {
		try {
			Connection con = Database.getConnection();
			PreparedStatement statement = con.prepareStatement(
					"SELECT * FROM dottori WHERE Tesserino=?");
			statement.setInt(1, tesserino);
			ResultSet result = statement.executeQuery();
			
			if (!result.isBeforeFirst()) {
				statement.close();
				con.close();
				return false;
			} else {
				statement.close();
				con.close();
				return true;
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return false;
	}
	
	public static ArrayList<String> getPazientiInCura(String tesserino) {
		ArrayList<String> paz = null;
		try {
			Connection conn = Database.getConnection();
			
			PreparedStatement stmt = conn.prepareStatement("SELECT pz.CfPaziente, pz.Nome, pz.Cognome "
					+ "FROM prestazioni AS pr "
					+ "INNER JOIN pazienti AS pz ON pz.CfPaziente = pr.CfPaziente "
					+ "WHERE pr.Tesserino = ?");
			
			stmt.setString(1, tesserino);
			paz = new ArrayList<>();
			
			ResultSet res = stmt.executeQuery();
			
			while(res.next()) {
				paz.add(res.getString("Nome") + " " +res.getString("Cognome") + " - cf: " +res.getString("CfPaziente"));
			}
			stmt.close();
			conn.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return paz;
	}
	
	public static int addDiagnosis(String cf, int tesserino, double altezza, double peso, Sangue sangue, String diagnosi) {
		int res = -1;
		try {
			Connection conn = Database.getConnection();
			
			PreparedStatement stmt = conn.prepareStatement("INSERT INTO diagnosi(CfPaziente, Tesserino, Altezza, Peso, Diagnosi) "
					+ "VALUES (?,?,?,?,?)");
			
			stmt.setString(1, cf);
			stmt.setInt(2, tesserino);
			stmt.setDouble(3, altezza);
			stmt.setDouble(4, peso);
			stmt.setString(5, diagnosi);
			res = stmt.executeUpdate();
			
			PreparedStatement stmt2 = conn.prepareStatement("UPDATE cartellecliniche SET Grupposanguigno = ? WHERE CfPaziente = ?");
			stmt2.setString(1, sangue.getSangue());
			stmt2.setString(2, cf);
			stmt2.executeUpdate();
			
			stmt.close();
			conn.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return res;
	}
	
	/*
	public static ArrayList<String> getDiagnosiVecchie(String cf) {
		ArrayList<String> diagnosi = null;
		try {
			Connection conn = Database.getConnection();
			diagnosi = new ArrayList<>();
			PreparedStatement stmt = conn.prepareStatement("SELECT d.Altezza, d.Peso, c.Grupposanguigno "
					+ "FROM diagnosi AS d "
					+ "INNER JOIN cartellecliniche AS c ON d.CfPaziente = c.CfPaziente "
					+ "WHERE d.CfPaziente = ? "
					+ "ORDER BY d.DataDiagnosi DESC "
					+ "LIMIT 1");
			stmt.setString(1, cf);
			
			ResultSet res = stmt.executeQuery();
			if(res.next()) {
				diagnosi.add(String.valueOf(res.getDouble("Altezza")));
				diagnosi.add(String.valueOf(res.getDouble("Peso")));
				if(res.getString("Grupposanguigno") != null)
					diagnosi.add(res.getString("Grupposanguigno"));
			}
			
		} catch (Exception e) {
			System.out.println(e);
		}
		
		return diagnosi;
	}
	*/
}