package controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class Database {

	public static Connection getConnection() throws Exception {
		//String url = "jdbc:mysql://127.0.0.1:3306/javaproject?serverTimezone=UTC";
		String url = "jdbc:mysql://yourheal.heliohost.org:3306/yourheal_yourhealth?serverTimezone=UTC";
		String username = "yourheal_username";
		String password = "sD3&2n,C4E;Z";

		Connection conn = DriverManager.getConnection(url, username, password);
		return conn;
	}

	public static void createTablePazienti() throws Exception {
		try {
			Connection conn = getConnection();
			PreparedStatement create = conn.prepareStatement(
					"CREATE TABLE IF NOT EXISTS `pazienti` (" + 
							"`Nome` VARCHAR(255) NOT NULL," + 
							"`Cognome` VARCHAR(255) NOT NULL," + 
							"`Sesso` VARCHAR(5) NOT NULL," + 
							"`LuogoNascita` VARCHAR(255) NOT NULL," + 
							"`DataNascita` DATE NOT NULL," + 
							"`CfPaziente` CHAR(16) PRIMARY KEY," + 
							"`Residenza` VARCHAR(255) NOT NULL" + 
					")");

			create.executeUpdate();
			System.out.println("Table Pazienti Created");

			create.close();
			conn.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static void createTableDottori() throws Exception {
		try {
			Connection conn = getConnection();
			PreparedStatement create = conn.prepareStatement(
					"CREATE TABLE IF NOT EXISTS `dottori` (" + 
							"`Nome` VARCHAR(255) NOT NULL," + 
							"`Cognome` VARCHAR(255) NOT NULL," + 
							"`Sesso` VARCHAR(5) NOT NULL," + 
							"`LuogoNascita` VARCHAR(255) NOT NULL, " + 
							"`DataNascita` DATE NOT NULL, " + 
							"`Tesserino` INT(255) NOT NULL PRIMARY KEY, " + 
							"`Ruolo` VARCHAR(255), " + 
							"`OrarioInizio` TIME NOT NULL, " + 
							"`OrarioFine` TIME NOT NULL, " + 
							"FOREIGN KEY(`Ruolo`) REFERENCES `ruoli`(`Nome`) " + 
							"ON DELETE CASCADE " + 
							"ON UPDATE CASCADE " + 
					")");
			create.executeUpdate();
			System.out.println("Table Dottori Created");
			create.close();
			conn.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static void createTableMacchinari() throws Exception {
		try {
			Connection conn = getConnection();
			PreparedStatement create = conn.prepareStatement(
					"CREATE TABLE IF NOT EXISTS `macchinari` ( " + 
							"`Codice` VARCHAR(255) PRIMARY KEY, " + 
							"`Tipo` varchar(255) NOT NULL, " + 
							"FOREIGN KEY (`Tipo`) REFERENCES `ruoli`(`CategoriaMacchinario`) " + 
							"ON DELETE CASCADE " + 
							"ON UPDATE CASCADE " + 
					")");

			create.executeUpdate();
			System.out.println("Table Macchinari Created");
			create.close();
			conn.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static void createTableAmbulatori() throws Exception {
		try {
			Connection conn = getConnection();
			PreparedStatement create = conn.prepareStatement(
					"CREATE TABLE IF NOT EXISTS `ambulatori` ( " + 
							"`Codice` VARCHAR(255) PRIMARY KEY, " + 
							"`Tipo` varchar(255) NOT NULL, " + 
							"FOREIGN KEY (`Tipo`) REFERENCES `ruoli`(`CategoriaAmbulatorio`) " + 
							"ON DELETE CASCADE " + 
							"ON UPDATE CASCADE " + 
					")");

			create.executeUpdate();
			create.close();
			conn.close();
			System.out.println("Table Ambulatori Created");
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static void createTablePrestazioni() throws Exception {
		try {
			Connection conn = getConnection();
			PreparedStatement create = conn.prepareStatement(
					"CREATE TABLE IF NOT EXISTS `prestazioni` ( " + 
							"`CfPaziente` CHAR(16), " + 
							"`Tesserino` INT(255), " + 
							"`Tipo` VARCHAR(255) NOT NULL, " + 
							"`Data` DATE NOT NULL, " + 
							"`Ora` TIME NOT NULL, " + 
							"`Stato` VARCHAR(255) NOT NULL, " + 
							"`Macchinario` VARCHAR(255) NOT NULL, " + 
							"`ID_Macchinario` VARCHAR(255), " + 
							"`Ambulatorio` VARCHAR(255) NOT NULL, " + 
							"`ID_Ambulatorio` VARCHAR(255), " + 
							"PRIMARY KEY(`CfPaziente`, `Tesserino`, `Data`, `Ora`), " + 
							"FOREIGN KEY(`CfPaziente`) REFERENCES `pazienti`(`CfPaziente`) " + 
							"ON DELETE CASCADE " + 
							"ON UPDATE CASCADE, " + 
							"FOREIGN KEY(`Tesserino`) REFERENCES `dottori`(`Tesserino`) " + 
							"ON DELETE CASCADE " + 
							"ON UPDATE CASCADE, " + 
							"FOREIGN KEY(`ID_Macchinario`) REFERENCES `macchinari`(`Codice`) " + 
							"ON DELETE CASCADE " + 
							"ON UPDATE CASCADE, " + 
							"FOREIGN KEY(`ID_Ambulatorio`) REFERENCES `ambulatori`(`Codice`) " + 
							"ON DELETE CASCADE " + 
							"ON UPDATE CASCADE " + 
					")");

			create.executeUpdate();
			System.out.println("Table Prestazioni Created");
			create.close();
			conn.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static void createTableCartelleCliniche() throws Exception {
		try {
			Connection conn = getConnection();
			PreparedStatement create = conn.prepareStatement(
					"CREATE TABLE IF NOT EXISTS `cartellecliniche` ( " + 
							"`CfPaziente` char(16) PRIMARY KEY, " + 
							"`Grupposanguigno` varchar(3) NOT NULL, " + 
							"FOREIGN KEY (`CfPaziente`) REFERENCES `pazienti`(`CfPaziente`) " + 
							"ON DELETE CASCADE " + 
							"ON UPDATE CASCADE " +  
					")");

			create.executeUpdate();
			create.close();
			conn.close();
			System.out.println("Table Diagnosi Created");
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static void createTableDiagnosi() throws Exception {
		try {
			Connection conn = getConnection();
			PreparedStatement create = conn.prepareStatement(
					"CREATE TABLE IF NOT EXISTS `diagnosi` ( " + 
							"`CfPaziente` char(16) NOT NULL, " + 
							"`Tesserino` int(255) NOT NULL, " + 
							"`DataDiagnosi` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP, " + 
							"`Altezza` double NOT NULL, " + 
							"`Peso` double NOT NULL, " + 
							"`Diagnosi` varchar(1020) NOT NULL, " + 
							"PRIMARY KEY (`CfPaziente`,`Tesserino`,`DataDiagnosi`), " + 
							"FOREIGN KEY (`CfPaziente`) REFERENCES `cartellecliniche`(`CfPaziente`) " + 
							"ON DELETE CASCADE " + 
							"ON UPDATE CASCADE, " + 
							"FOREIGN KEY (`Tesserino`) REFERENCES `dottori`(`Tesserino`) " + 
							"ON DELETE CASCADE " + 
					")");

			create.executeUpdate();
			create.close();
			conn.close();
			System.out.println("Table Diagnosi Created");
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static void createTableRuoli() throws Exception {
		try {
			Connection conn = getConnection();
			PreparedStatement create = conn.prepareStatement(
					"CREATE TABLE IF NOT EXISTS `ruoli` ( " + 
							"  `Nome` varchar(255) NOT NULL, " + 
							"  `CategoriaAmbulatorio` varchar(255) NOT NULL, " + 
							"  `CategoriaMacchinario` varchar(255) NOT NULL, " + 
							"  PRIMARY KEY (`Nome`), " + 
							"  UNIQUE KEY `Nome` (`Nome`), " + 
							"  UNIQUE KEY `unique_amb` (`CategoriaAmbulatorio`), " + 
							"  UNIQUE KEY `CategoriaMacchinario` (`CategoriaMacchinario`) " + 
					")");

			create.executeUpdate();
			create.close();
			conn.close();
			System.out.println("Table Ruoli Created");
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}