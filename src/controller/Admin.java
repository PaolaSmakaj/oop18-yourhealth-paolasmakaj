package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import model.Ambulatorio;
import model.AmbulatorioImpl;
import model.Dottore;
import model.DottoreImpl;
import model.Macchinario;
import model.MacchinarioImpl;
import model.Paziente;
import model.PazienteImpl;
import model.Prestazione;
import model.PrestazioneImpl;
import util.Enums;

public class Admin {

	public static int addPaziente(Paziente P) {
		int up = -1;
		try {
			Connection con = Database.getConnection();

			PreparedStatement posted = con.prepareStatement(
					"INSERT INTO pazienti (Nome,Cognome,Sesso,LuogoNascita,DataNascita,CfPaziente,Residenza) VALUES (?,?,?,?,?,?,?)");
			
			posted.setString(1, P.getNome());
			posted.setString(2, P.getCognome());
			posted.setString(3, P.getSesso());
			posted.setString(4, P.getLuogoNascita());
			posted.setString(5, P.getDataNascita().toString());
			posted.setString(6, P.getCodicefiscale());
			posted.setString(7, P.getResidenza());

			up = posted.executeUpdate();
			
			posted.close();
			con.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return up;
	}

	public static void removePaziente(String codicefiscale) {
		try {
			Connection con = Database.getConnection();
			
			PreparedStatement posted = con
					.prepareStatement("DELETE FROM pazienti WHERE CfPaziente = ?");
			posted.setString(1, codicefiscale);

			posted.executeUpdate();
			posted.close();
			con.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public static ArrayList<Paziente> getListaPazienti() {
		try {
			Connection con = Database.getConnection();
			
			PreparedStatement statement = con.prepareStatement("SELECT * FROM pazienti ORDER BY Cognome");

			ResultSet result = statement.executeQuery();

			ArrayList<Paziente> array = new ArrayList<Paziente>();
			while (result.next()) {
				Paziente P = new PazienteImpl(result.getString("Nome"), result.getString("Cognome"),
						Enums.Sesso.getFromString(result.getString("Sesso")), result.getString("LuogoNascita"),
						LocalDate.parse(result.getString("DataNascita"), DateTimeFormatter.ofPattern("yyyy-MM-dd")),
						result.getString("CfPaziente"), result.getString("Residenza"));

				array.add(P);
			}
			statement.close();
			con.close();
			return array;

		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}

	public static int addDottore(Dottore D) {
		int up = -1;
		try {
			Connection con = Database.getConnection();
			

			PreparedStatement posted = con.prepareStatement(
					"INSERT INTO dottori (Nome,Cognome,Sesso,LuogoNascita,DataNascita,Tesserino,Ruolo,OrarioInizio,OrarioFine)"
					+ " VALUES (?,?,?,?,?,?,?,?,?)");
			
			posted.setString(1, D.getNome());
			posted.setString(2, D.getCognome());
			posted.setString(3, D.getSesso());
			posted.setString(4, D.getLuogoNascita());
			posted.setString(5, D.getDataNascita().toString());
			posted.setInt(6, D.getTesserino());
			posted.setString(7, D.getRuolo());
			posted.setString(8, D.getOrarioInizio().toString());
			posted.setString(9, D.getOrarioFine().toString());
			
			up = posted.executeUpdate();
			con.close();
			posted.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return up;
	}

	public static int removeDottore(int tesserino) {
		int up = -1;
		try {
			Connection con = Database.getConnection();
			PreparedStatement posted = con.prepareStatement("DELETE FROM dottori WHERE Tesserino = ?");
			
			posted.setInt(1, tesserino);

			up = posted.executeUpdate();
			posted.close();
			con.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return up;
	}

	public static ArrayList<Dottore> getListaDottori() {
		try {
			Connection con = Database.getConnection();
			PreparedStatement statement = con.prepareStatement("SELECT * FROM dottori ORDER BY Cognome");

			ResultSet result = statement.executeQuery();

			ArrayList<Dottore> array = new ArrayList<Dottore>();
			while (result.next()) {
				Dottore D = new DottoreImpl(result.getString("Nome"), result.getString("Cognome"),
						Enums.Sesso.getFromString(result.getString("Sesso")), result.getString("LuogoNascita"),
						LocalDate.parse(result.getString("DataNascita"), DateTimeFormatter.ofPattern("yyyy-MM-dd")),
						result.getInt("Tesserino"), Enums.Ruolo.getFromString(result.getString("Ruolo")),
						LocalTime.parse(result.getString("OrarioInizio"), DateTimeFormatter.ofPattern("HH:mm:ss")),
						LocalTime.parse(result.getString("OrarioFine"), DateTimeFormatter.ofPattern("HH:mm:ss")));
				array.add(D);
			}
			System.out.println("Lista Dottori Selezionata");
			statement.close();
			con.close();
			return array;

		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}

	public static int addMacchinario(Macchinario M) {
		int up = -1;
		try {
			Connection con = Database.getConnection();
			PreparedStatement posted = con
					.prepareStatement("INSERT INTO macchinari (Codice,Tipo) VALUES (?,?)");
			
			posted.setString(1, M.getCodice());
			posted.setString(2, M.getTipo());
			up = posted.executeUpdate();
			con.close();
			posted.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return up;
	}

	public static void removeMacchinario(String codice){
		try {
			Connection con = Database.getConnection();
			PreparedStatement posted = con.prepareStatement("DELETE FROM macchinari WHERE Codice = ?");
			posted.setString(1, codice);
			posted.executeUpdate();
			con.close();
			posted.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public static ArrayList<Macchinario> getListaMacchinari() {
		try {
			Connection con = Database.getConnection();
			PreparedStatement statement = con.prepareStatement("SELECT * FROM macchinari ORDER BY Tipo");

			ResultSet result = statement.executeQuery();

			ArrayList<Macchinario> array = new ArrayList<Macchinario>();
			while (result.next()) {
				Macchinario M = new MacchinarioImpl.Builder().codice(result.getString("Codice"))
						.tipo(Enums.TipoMacchinario.getFromString(result.getString("Tipo")))
						.build();
				array.add(M);
			}
			System.out.println("Lista Macchinari Selezionata");
			con.close();
			statement.close();
			return array;

		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}

	public static int addAmbulatorio(Ambulatorio A) {
		int up = -1;
		try {
			Connection con = Database.getConnection();
			PreparedStatement posted = con
					.prepareStatement("INSERT INTO ambulatori (Codice,Tipo) VALUES (?,?)");
			
			posted.setString(1, A.getCodice());
			posted.setString(2, A.getTipo());

			up = posted.executeUpdate();
			con.close();
			posted.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return up;
	}

	public static void removeAmbulatorio(String codice) {
		try {
			Connection con = Database.getConnection();
			PreparedStatement posted = con.prepareStatement("DELETE FROM ambulatori WHERE Codice = ?");
			
			posted.setString(1, codice);

			posted.executeUpdate();
			posted.close();
			con.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public static ArrayList<Ambulatorio> getListaAmbulatori(){
		try {
			Connection con = Database.getConnection();
			PreparedStatement statement = con.prepareStatement("SELECT * FROM ambulatori ORDER BY Tipo");

			ResultSet result = statement.executeQuery();

			ArrayList<Ambulatorio> array = new ArrayList<Ambulatorio>();
			while (result.next()) {
				Ambulatorio A = new AmbulatorioImpl.Builder().codice(result.getString("Codice"))
						.tipo(Enums.TipoAmbulatorio.getFromString(result.getString("Tipo"))).build();
				array.add(A);
			}
			System.out.println("Lista Ambulatori Selezionata");
			statement.close();
			con.close();
			return array;

		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}

	public static void addPrestazione(Prestazione Pr){
			Connection con;
			try {
				con = Database.getConnection();
				PreparedStatement posted = con.prepareStatement(
						"INSERT INTO prestazioni (CfPaziente,Tesserino,Tipo,Data,Ora,Stato,Macchinario,ID_Macchinario,Ambulatorio,ID_Ambulatorio)"
						+" VALUES (?,?,?,?,?,?,?,?,?,?)");
				
				posted.setString(1, Pr.getPaziente());
				posted.setInt(2, Pr.getDottore());
				posted.setString(3, Pr.getTipo());
				posted.setString(4, Pr.getData().toString());
				posted.setString(5, Pr.getOra().toString());
				posted.setString(6, Pr.getStato());
				posted.setString(7, Pr.getMacchinario().split("- cod.")[0].trim());
				posted.setString(8, Pr.getMacchinario().split("- cod.")[1].trim());
				posted.setString(9, Pr.getAmbulatorio().split("- cod.")[0].trim());
				posted.setString(10, Pr.getAmbulatorio().split("- cod.")[1].trim());
				posted.executeUpdate();
				
				posted.close();
				con.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
	}

	public static int removePrestazione(String codicefiscale, int tesserino, LocalDate data, LocalTime ora) {
		int up = -1;
		try {
			Connection con = Database.getConnection();
			PreparedStatement posted = con
					.prepareStatement("DELETE FROM prestazioni WHERE CfPaziente = ? and Tesserino = ?"
							+" and Data = ? and Ora = ?");
			posted.setString(1, codicefiscale);
			posted.setInt(2, tesserino);
			posted.setString(3, data.toString());
			posted.setString(4, ora.toString());
			up = posted.executeUpdate();
			
			con.close();
			posted.close();
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return up;
	}

	public static ArrayList<Prestazione> getListaPrestazioni() {
		try {
			Connection con = Database.getConnection();
			PreparedStatement statement = con.prepareStatement("SELECT * FROM prestazioni ORDER BY Data");

			ResultSet result = statement.executeQuery();

			ArrayList<Prestazione> array = new ArrayList<Prestazione>();
			while (result.next()) {

				Prestazione Pr = new PrestazioneImpl.Builder().paziente(result.getString("CfPaziente")).dottore(result.getInt("Tesserino"))
						.tipoprestazione(Enums.TipoPrestazione.getFromString(result.getString("Tipo")))
						.data(LocalDate.parse(result.getString("Data"), DateTimeFormatter.ofPattern("yyyy-MM-dd")))
						.ora(LocalTime.parse(result.getString("Ora"), DateTimeFormatter.ofPattern("HH:mm:ss")))
						.stato(Enums.Stato.getFromString(result.getString("Stato"))).macchinario(result.getString("Macchinario"))
						.ambulatorio(result.getString("Ambulatorio")).build();

				array.add(Pr);
			}
			System.out.println("Lista Prestazioni Selezionata");
			statement.close();
			con.close();
			return array;

		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}

	public static Macchinario getMacchinarioFromDB(String codice) {
		try {
			Connection con = Database.getConnection();
			PreparedStatement statement = con.prepareStatement("SELECT * FROM macchinari WHERE Codice = ?");
			
			statement.setString(1, codice);

			ResultSet result = statement.executeQuery();


			Macchinario M = new MacchinarioImpl.Builder()
					.codice(result.getString("Codice"))
					.tipo(Enums.TipoMacchinario.getFromString(result.getString("Tipo")))
					.build();
			
			statement.close();
			con.close();
			return M;

		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}

	public static Ambulatorio getAmbulatorioFromDB(String codice) {
		try {
			Connection con = Database.getConnection();
			PreparedStatement statement = con.prepareStatement("SELECT * FROM ambulatori WHERE Codice = ?");

			ResultSet result = statement.executeQuery();

			Ambulatorio A = new AmbulatorioImpl.Builder().codice(result.getString("Codice"))
					.tipo(Enums.TipoAmbulatorio.getFromString(result.getString("Tipo")))
					.build();
			
			statement.close();
			con.close();
			return A;

		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}

	public static boolean checkDisponibilitÓPrestazione(int tesserino, LocalDate data, LocalTime ora, String codiceMacch,
			String codiceAmb) {
		try {
			Connection con = Database.getConnection();
			PreparedStatement statement = con.prepareStatement(
					"SELECT * FROM prestazioni WHERE Data = ? AND Ora = ?");
			
			statement.setString(1, data.toString());
			statement.setString(2, ora.toString());

			ResultSet result = statement.executeQuery();

			ArrayList<Prestazione> array = new ArrayList<Prestazione>();
			while (result.next()) {

				Prestazione Pr = new PrestazioneImpl.Builder().paziente(result.getString("CfPaziente")).dottore(result.getInt("Tesserino"))
						.tipoprestazione(Enums.TipoPrestazione.getFromString(result.getString("Tipo")))
						.data(LocalDate.parse(result.getString("Data"), DateTimeFormatter.ofPattern("yyyy-MM-dd")))
						.ora(LocalTime.parse(result.getString("Ora"), DateTimeFormatter.ofPattern("HH:mm:ss")))
						.stato(Enums.Stato.getFromString(result.getString("Stato"))).macchinario(result.getString("Macchinario"))
						.ambulatorio(result.getString("Ambulatorio")).build();

				array.add(Pr);
			}
			
			statement.close();
			con.close();
			
			for (Prestazione object : array) {
				if (object.getDottore() == tesserino || object.getMacchinario() == codiceMacch
						|| object.getAmbulatorio() == codiceAmb) {
					return false;
				}
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return true;
	}
	
	public static ArrayList<String> getRuoliDottore() {
		ArrayList<String> ruoli = null;
		try {
			Connection conn = Database.getConnection();
			String query = "SELECT Nome FROM ruoli";
			
			PreparedStatement stmt = conn.prepareStatement(query);
			ruoli = new ArrayList<>();
			
			ResultSet res = stmt.executeQuery();
			
			while(res.next()) {
				ruoli.add(res.getString("Nome"));
			}
			stmt.close();
			conn.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return ruoli;
	}

	public static ArrayList<String> getTipiAmbulatorio() {
		ArrayList<String> tipi = null;
		try {
			Connection conn = Database.getConnection();
			String query = "SELECT CategoriaAmbulatorio FROM ruoli";
			
			PreparedStatement stmt = conn.prepareStatement(query);
			tipi = new ArrayList<>();
			
			ResultSet res = stmt.executeQuery();
			
			while(res.next()) {
				tipi.add(res.getString("CategoriaAmbulatorio"));
			}
			stmt.close();
			conn.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return tipi;
	}
	
	public static ArrayList<String> getTipiMacchinario() {
		ArrayList<String> tipi = null;
		try {
			Connection conn = Database.getConnection();
			String query = "SELECT CategoriaMacchinario FROM ruoli";
			
			PreparedStatement stmt = conn.prepareStatement(query);
			tipi = new ArrayList<>();
			
			ResultSet res = stmt.executeQuery();
			
			while(res.next()) {
				tipi.add(res.getString("CategoriaMacchinario"));
			}
			stmt.close();
			conn.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return tipi;
	}
	
	public static ArrayList<String> getMacchinariSelezionabili(String tesserino) {
		ArrayList<String> macch = null;
		try {
			Connection conn = Database.getConnection();
			
			PreparedStatement stmt = conn.prepareStatement("SELECT macc.Tipo, macc.Codice "
					+ "FROM `dottori` AS doc "
					+ "INNER JOIN `ruoli` AS rm ON doc.Ruolo = rm.Nome "
					+ "INNER JOIN `macchinari` AS macc ON macc.Tipo = rm.CategoriaMacchinario "
					+ "WHERE doc.Tesserino = ?");
			
			stmt.setString(1, tesserino);
			macch = new ArrayList<>();
			
			ResultSet res = stmt.executeQuery();
			
			while(res.next()) {
				macch.add(res.getString("Tipo")+ " - cod. " +res.getString("Codice"));
			}
			stmt.close();
			conn.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		
		return macch;
	}
	
	public static ArrayList<String> getAmbulatoriSelezionabili(String tesserino) {
		ArrayList<String> amb = null;
		try {
			Connection conn = Database.getConnection();
			
			PreparedStatement stmt = conn.prepareStatement("SELECT amb.Tipo, amb.Codice "
					+ "FROM `dottori` AS doc "
					+ "INNER JOIN `ruoli` AS r ON r.Nome = doc.Ruolo "
					+ "INNER JOIN `ambulatori` AS amb ON amb.Tipo = r.CategoriaAmbulatorio "
					+ "WHERE doc.Tesserino = ?");
			
			stmt.setString(1, tesserino);
			amb = new ArrayList<>();
			
			ResultSet res = stmt.executeQuery();
			
			while(res.next()) {
				amb.add(res.getString("Tipo")+ " - cod. " +res.getString("Codice"));
			}
			stmt.close();
			conn.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		
		return amb;
	}
	
	
}