package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Optional;

import model.CartellaClinica;
import model.CartellaClinicaImpl;
import model.Diagnosi;

public class Account {
	
	public static CartellaClinica getCartellaClinica(String cf) {
		CartellaClinica cc = null;
		try {
			
			Connection conn = Database.getConnection();
			
			PreparedStatement stmt = conn.prepareStatement("SELECT *"
					+ "FROM cartellecliniche "
					+ "WHERE CfPaziente = ?");
			stmt.setString(1, cf);
			
			ResultSet res = stmt.executeQuery();
			res.next();
			
			cc = new CartellaClinicaImpl(cf, Optional.ofNullable(res.getString("Grupposanguigno")));
			
			stmt = conn.prepareStatement("SELECT Tesserino, DataDiagnosi, Altezza, Peso, Diagnosi "
					+ "FROM diagnosi "
					+ "WHERE CfPaziente = ?");
			stmt.setString(1, cf);
			
			res = stmt.executeQuery();
			
			while(res.next()) {
				cc.aggiungiDiagnosi(new Diagnosi(res.getInt("Tesserino"), 
						res.getString("DataDiagnosi"), 
						res.getDouble("Altezza"), 
						res.getDouble("Peso"), 
						res.getString("Diagnosi")));
			}
			stmt.close();
			conn.close();
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
		
		return cc;
	}
}
