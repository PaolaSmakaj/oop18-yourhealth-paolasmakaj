package view;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.util.Optional;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;

import controller.Account;
import controller.AccountDottore;
import controller.AccountPaziente;
import model.CartellaClinica;
import model.Diagnosi;
import model.Paziente;
import util.Enums;
import util.Enums.Sangue;

public class CartellaClinicaForm extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3731163138593856647L;
    private GUI fac = new GUIFactory();
    private final Float font = 20.0f;
    private Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    private int width = (int) screenSize.getWidth();
   
    public CartellaClinicaForm(String cf, int tesserino) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        frame.setPreferredSize(new Dimension(width / 3, 418));
        frame.setLocationByPlatform(true);
        frame.setLayout(new BorderLayout());
        frame.setTitle("Cartella clinica di "+cf);
        frame.setResizable(false);

        JPanel canvas = fac.createBoxPanel();
        frame.add(canvas, BorderLayout.WEST);
        JPanel canvas2 = fac.createBoxPanel();
        frame.add(canvas2, BorderLayout.CENTER);
        JPanel canvas3 = fac.createFlowPanel();
        frame.add(canvas3,  BorderLayout.EAST);
        
        JLabel labelName = fac.createLabelRight("Nome: ", font);
		canvas.add(labelName);
		JTextField textName = fac.createTextField();
		canvas2.add(textName);

		JLabel labelSurname = fac.createLabelRight("Cognome: ", font);
		canvas.add(labelSurname);
		JTextField textSurname = fac.createTextField();
		canvas2.add(textSurname);

		JLabel labelSex = fac.createLabelRight("Sesso: ", font);
		canvas.add(labelSex);
		JTextField textSex = fac.createTextField();
		canvas2.add(textSex);

		JLabel labelLuogoNascita = fac.createLabelRight("Luogo Nascita: ", font);
		canvas.add(labelLuogoNascita);
		JTextField textLuogoNascita = fac.createTextField();
		canvas2.add(textLuogoNascita);

		JLabel labelDataNascita = fac.createLabelRight("Data Nascita: ", font);
		canvas.add(labelDataNascita);
		JTextField textDataNascita = fac.createTextField();
		canvas2.add(textDataNascita);

		JLabel labelCodFis = fac.createLabelRight("Codice Fiscale: ", font);
		canvas.add(labelCodFis);
		JTextField textCodFis = fac.createTextField();
		canvas2.add(textCodFis);

		JLabel labelResidenza = fac.createLabelRight("Residenza: ", font);
		canvas.add(labelResidenza);
		JTextField textResidenza = fac.createTextField();
		canvas2.add(textResidenza);
		
		Paziente pat = AccountPaziente.getPazienteFromDB(cf);
        textName.setText(pat.getNome());
        textSurname.setText(pat.getCognome());
        textCodFis.setText(pat.getCodicefiscale());
        textSex.setText(pat.getSesso());
        textLuogoNascita.setText(pat.getLuogoNascita());
        textDataNascita.setText(pat.getDataNascita().toString());
        textResidenza.setText(pat.getResidenza());
         
        textName.setEditable(false);
		textSurname.setEditable(false);
		textCodFis.setEditable(false);
		textSex.setEditable(false);
		textLuogoNascita.setEditable(false);
		textDataNascita.setEditable(false);
		textResidenza.setEditable(false);
        
        JLabel labelBlood = fac.createLabelRight("Gruppo Sanguigno: ", font);
        canvas.add(labelBlood);
        JComboBox<String> textBlood = fac.createCombo(Enums.Sangue.getValoriSangue());
		canvas2.add(textBlood);
		
		JLabel labelHeight = fac.createLabelRight("Altezza: ", font);
        canvas.add(labelHeight);
        JTextField textHeight = fac.createTextField();
        canvas2.add(textHeight);
        
        JLabel labelWeight = fac.createLabelRight("Peso: ", font);
        canvas.add(labelWeight);
        JTextField textWeight = fac.createTextField();
        canvas2.add(textWeight);
        
        JLabel labelDiagnosis = fac.createLabelRight("Diagnosi: ", font);
        canvas.add(labelDiagnosis);
        JTextArea textDiagnosis = new JTextArea(10, 5);
        Border border = BorderFactory.createLineBorder(Color.GRAY, 1);
        textDiagnosis.setBorder(border);
        textDiagnosis.setLineWrap(true);
        JScrollPane sp = new JScrollPane(textDiagnosis);
        canvas2.add(sp);
        
        CartellaClinica cc = Account.getCartellaClinica(cf);
        Optional<Diagnosi> d = cc.getUltimaDiagnosi();
        if(cc.getGruppoSanguigno().isPresent())
        	textBlood.setSelectedItem(cc.getGruppoSanguigno().get());
        
        if(d.isPresent()) {
        	textHeight.setText(String.valueOf(d.get().getAltezza()));
        	textWeight.setText(String.valueOf(d.get().getPeso()));
        }
        
        /*
        ArrayList<String> oldDiagnosis = AccountDottore.getDiagnosiVecchie(cf);
        if(oldDiagnosis.size() > 1) {
        	textHeight.setText(oldDiagnosis.get(0));
        	textWeight.setText(oldDiagnosis.get(1));
        	if(oldDiagnosis.size() == 3) {
        		textBlood.setSelectedItem(oldDiagnosis.get(2));
        	}
        }
        */

        JButton confirm = new JButton("Salva");
        confirm.setFont(new Font("Calibri", Font.PLAIN,18));
        confirm.setBackground(Color.darkGray);
        confirm.setForeground(Color.white);
        confirm.addActionListener(a -> {
        	//type = Enums.TipoAmbulatorio.getFromString(textBlood.getSelectedItem().toString());
        	//id = textId.getText();
        	double height = 0.0, weight = 0.0;
        	try {
        		height = Double.parseDouble(textHeight.getText());
            	weight = Double.parseDouble(textWeight.getText());
            	if(height <= 0 || weight <= 0)
            		throw new NumberFormatException();
        	} catch (NullPointerException e) {
            	JOptionPane.showMessageDialog(this,"Compila tutti i campi",
    					"Errore",JOptionPane.ERROR_MESSAGE);
            	return;
        	} catch (NumberFormatException ne) {
            	JOptionPane.showMessageDialog(this,"Valori non validi",
    					"Errore",JOptionPane.ERROR_MESSAGE);
            	return;
        	}
        	
        	String diagnosis = textDiagnosis.getText();
        	Sangue blood = Sangue.getFromString(textBlood.getSelectedItem().toString());
        	if(diagnosis.isEmpty()) {
            	JOptionPane.showMessageDialog(this,"Compila tutti i campi",
    					"Errore",JOptionPane.ERROR_MESSAGE);
            	return;
        	} else {
        		int ret = AccountDottore.addDiagnosis(pat.getCodicefiscale(), tesserino, height, weight, blood, diagnosis);
        		if(ret < 0) {
                	JOptionPane.showMessageDialog(this,"Errore nell'inserimento nel DB",
        					"Errore",JOptionPane.ERROR_MESSAGE);
                	return;
        		} else if(ret == 0) {
                	JOptionPane.showMessageDialog(this,"Non sono stati effettuati nuovi inserimenti",
        					"Attenzione",JOptionPane.WARNING_MESSAGE);
                	return;
        		} else {
                	JOptionPane.showMessageDialog(this,"Valori inseriti correttamente",
        					"Ok",JOptionPane.INFORMATION_MESSAGE);
                	frame.dispose();
        		}
        	}
			/*else {
				JOptionPane.showMessageDialog(frame, "Elemento aggiunto correttamente");
				frame.dispose();
			}*/
        });
        canvas3.add(confirm);
        frame.setVisible(true);
        frame.pack();
    }
}

