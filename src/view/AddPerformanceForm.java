package view;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controller.Admin;
import model.Dottore;
import model.Paziente;
import model.Prestazione;
import model.PrestazioneImpl;
import util.Enums;
import util.Enums.Stato;
import util.Enums.TipoPrestazione;

public class AddPerformanceForm extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2970713926117414003L;
	
	private Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    private int width = (int) screenSize.getWidth();
    private int height = (int) screenSize.getHeight();
    private GUI fac = new GUIFactory();
    private String codFis, Mach, Amb;
    private TipoPrestazione tipo;
    private Stato stato;
    private int tesserinoDottore;
	private LocalDate data;
	private LocalTime ora;
    private final Float font = 20.0f;
   
    public AddPerformanceForm(){
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        frame.setSize(width / 3, height / 3);
        frame.setLocationByPlatform(true);
        frame.setLayout(new BorderLayout());
        frame.setTitle("Aggiungi prestazione");
        frame.setResizable(false);

        JButton confirm = new JButton("Salva");
        JPanel canvas = fac.createBoxPanel();
        frame.add(canvas, BorderLayout.WEST);
        JPanel canvas2 = fac.createBoxPanel();
        frame.add(canvas2, BorderLayout.CENTER);
        JPanel canvas3 = fac.createFlowPanel();
        frame.add(canvas3,  BorderLayout.EAST);

        JLabel labelCodFis = fac.createLabelRight("Codice fiscale: ", font);
        canvas.add(labelCodFis);
        ArrayList<Paziente> pats = Admin.getListaPazienti();
        String[] cods = new String[pats.size()];
        for(int i = 0; i < pats.size(); i++) {
        	cods[i] = pats.get(i).getCodicefiscale();
        }
        JComboBox<String> textCodFis = fac.createCombo(cods);
        canvas2.add(textCodFis);
        if(pats.isEmpty()) {
			textCodFis.addItem(new String("Nessun paziente trovato"));
			textCodFis.setEnabled(false);
			confirm.setEnabled(false);
        }

        JLabel labelDoc = fac.createLabelRight("Tesserino dottore: ", font);
        canvas.add(labelDoc);
        ArrayList<Dottore> docs = Admin.getListaDottori();
        String[] tess = new String[docs.size()];
        for(int i = 0; i < docs.size(); i++) {
        	tess[i] = "" +docs.get(i).getTesserino();
        }
        JComboBox<String> textDoc = fac.createCombo(tess);
        canvas2.add(textDoc);
        if(docs.isEmpty()) {
			textDoc.addItem(new String("Nessun dottore trovato"));
			textDoc.setEnabled(false);
			confirm.setEnabled(false);
        }
        
        JLabel labelType = fac.createLabelRight("Tipo Prestazione: ", font);
		canvas.add(labelType);
		JComboBox<String> textType = fac.createCombo(Enums.TipoPrestazione.getValoriPrestazioni());
		canvas2.add(textType);
		
		JLabel labelDate = fac.createLabelRight("Data Prestazione: ", font);
		canvas.add(labelDate);
		JTextField textDate = fac.createTextField();
		canvas2.add(textDate);
		
		JLabel labelTime = fac.createLabelRight("Ora Prestazione: ", font);
		canvas.add(labelTime);
		JTextField textTime = fac.createTextField();
		canvas2.add(textTime);
        
        JLabel labelMach = fac.createLabelRight("Macchinario: ", font);
        canvas.add(labelMach);
        
        ArrayList<String> macchinari = Admin.getMacchinariSelezionabili(textDoc.getSelectedItem().toString());
        String[] descrMac = new String[macchinari.size()];
        macchinari.toArray(descrMac);
        final JComboBox<String> textMach = fac.createCombo(descrMac);
		canvas2.add(textMach);
		if(macchinari.isEmpty()) {
			textMach.addItem(new String("Nessun macchinario trovato"));
			textMach.setEnabled(false);
			confirm.setEnabled(false);
		}
		
		ArrayList<String> amb = Admin.getAmbulatoriSelezionabili(textDoc.getSelectedItem().toString());
		String[] descrAmb = new String[amb.size()];
		amb.toArray(descrAmb);
        JLabel labelAmb = fac.createLabelRight("Ambulatorio: ", font);
        canvas.add(labelAmb);
        final JComboBox<String> textAmb = fac.createCombo(descrAmb);
        canvas2.add(textAmb);
		if(amb.isEmpty()) {
			textAmb.addItem(new String("Nessun ambulatorio trovato"));
			textAmb.setEnabled(false);
			confirm.setEnabled(false);
		}
        
		//adding item change listener to doc combo in order to update the available machines
        textDoc.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent event) {
				if(event.getStateChange() == ItemEvent.SELECTED) {
					String tesserino = event.getItem().toString();
					ArrayList<String> macchinari = Admin.getMacchinariSelezionabili(tesserino);
					ArrayList<String> amb = Admin.getAmbulatoriSelezionabili(tesserino);
					
					String[] descrMach = new String[macchinari.size()];
					String[] descrAmb = new String[amb.size()];
					
					macchinari.toArray(descrMach);
					amb.toArray(descrAmb);
					
					textMach.removeAllItems();
					if(!macchinari.isEmpty()) {
						textMach.setEnabled(true);
						for(int i = 0; i < descrMach.length; i++) {
							textMach.addItem(descrMach[i]);
						}
					}
					else {
						textMach.addItem(new String("Nessun macchinario trovato"));
						textMach.setEnabled(false);
					}
					
					textAmb.removeAllItems();
					if(!amb.isEmpty()) {
						textAmb.setEnabled(true);
						for(int i = 0; i < descrAmb.length; i++) {
							textAmb.addItem(descrAmb[i]);
						}
					} else {
						textAmb.addItem(new String("Nessun ambulatorio trovato"));
						textAmb.setEnabled(false);
					}
					
					if(!amb.isEmpty() && !macchinari.isEmpty()) {
						confirm.setEnabled(true);
					} else {
						confirm.setEnabled(false);
					}
				}
			}
		});


        confirm.setFont(new Font("Calibri", Font.PLAIN,18));
        confirm.setBackground(Color.darkGray);
        confirm.setForeground(Color.white);
        confirm.addActionListener(a -> {
            try {
            	if(textDate.getText().isEmpty() || textTime.getText().isEmpty()) {
            		throw new IllegalArgumentException();
            	}
                codFis = textCodFis.getSelectedItem().toString();
                tesserinoDottore = Integer.parseInt(textDoc.getSelectedItem().toString());
                tipo =  Enums.TipoPrestazione.getFromString(textType.getSelectedItem().toString());
                stato = Enums.Stato.PRENOTATO;
                data = LocalDate.parse(textDate.getText(), DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                ora = LocalTime.parse(textTime.getText(), DateTimeFormatter.ofPattern("HH:mm"));
                Mach = textMach.getSelectedItem().toString();
                Amb = textAmb.getSelectedItem().toString();

                Prestazione P = new PrestazioneImpl(codFis, tesserinoDottore, tipo,
                		data, ora, stato, Mach, Amb);
                if(Admin.checkDisponibilit�Prestazione(tesserinoDottore, data, ora, Mach.split("cod. ")[1], Amb.split("cod. ")[1])) {
                    Admin.addPrestazione(P);
                    JOptionPane.showMessageDialog(frame, "Prestazione aggiunta correttamente");
                    frame.dispose();
                }
                else {
                	JOptionPane.showMessageDialog(this,"Impossibile creare la prestazione, 1 o pi� elementi gi� occupati per la data e l'ora scelte",
        					"Errore",JOptionPane.ERROR_MESSAGE); 
                }
            } catch (IllegalArgumentException e) {
                JOptionPane.showMessageDialog(frame, "Compilare tutti i campi", "Errore", JOptionPane.ERROR_MESSAGE);
            } catch (IllegalStateException e) {
                JOptionPane.showMessageDialog(frame, e.getMessage());
            } catch(DateTimeParseException e) {
            	JOptionPane.showMessageDialog(frame, "Inserire la data nel formato dd/MM/yyyy e l'orario nel formato HH:mm", "Errore", JOptionPane.ERROR_MESSAGE);
			} catch(Exception e) {
				e.printStackTrace();
			}
        });
        canvas3.add(confirm);
        frame.setVisible(true);
        frame.pack();
    }
}
