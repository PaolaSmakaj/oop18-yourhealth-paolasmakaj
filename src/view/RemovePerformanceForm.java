package view;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import controller.Admin;
import model.Prestazione;

public class RemovePerformanceForm extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6274630894009026575L;
	private Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    private int width = (int) screenSize.getWidth();
    private int height = (int) screenSize.getHeight();
    private GUI fac = new GUIFactory();
    private String codicefiscale;
    private int tesserino;
    private LocalDate data;
    private LocalTime ora;
    private final float font = 20.0f;
    
    
    public RemovePerformanceForm() {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        frame.setSize(width / 3, height / 3);
        frame.setLocationByPlatform(true);
        frame.setLayout(new BorderLayout());
        frame.setTitle("Rimuovi Prestazione");
        frame.setResizable(false);
        
        
      //Panels
        JPanel canvas = fac.createBoxPanel();
        frame.add(canvas, BorderLayout.WEST);
        JPanel canvas2 = fac.createBoxPanel();
        frame.add(canvas2, BorderLayout.CENTER);
        JPanel canvas3 = fac.createFlowPanel();
        frame.add(canvas3,  BorderLayout.EAST);
        
        JButton confirm = new JButton("Rimuovi");
        
        JLabel labelP = fac.createLabelRight("Prestazione: ", font);
        canvas.add(labelP);
        
        JComboBox<String> choiceCombo;
        ArrayList<Prestazione> perf = Admin.getListaPrestazioni(); 
        if(perf != null && perf.size() != 0) {
        	String[] desc = new String[perf.size()];
            
            for(Prestazione p : perf) {
            	desc[perf.indexOf(p)] = "Paziente: " + p.getPaziente()+ " Dottore: " +p.getDottore()+ " Data: " +p.getData()+ " Ora: " +p.getOra()+ " Macchinario: " + p.getMacchinario() + " Ambulatorio: " +p.getAmbulatorio();
            }
            choiceCombo = fac.createCombo(desc);
        } else {
        	if(perf == null) {
        		choiceCombo = fac.createCombo(new String[] {"Errore nell'ottenimento delle prestazioni dal DB"});
        	} else {
        		choiceCombo = fac.createCombo(new String[] {"Nessuna prestazione trovata"});
        	}
        	
        	choiceCombo.setEnabled(false);
        	confirm.setEnabled(false);
        }
        
        choiceCombo.setEditable(false);
        canvas2.add(choiceCombo);

        confirm.setFont(new Font("Calibri", Font.PLAIN,18));
        confirm.setBackground(Color.darkGray);
        confirm.setForeground(Color.white);
        confirm.addActionListener(a -> {
        	
        	codicefiscale = choiceCombo.getSelectedItem().toString().split(" ")[1].trim();
        	tesserino = Integer.parseInt(choiceCombo.getSelectedItem().toString().split(" ")[3].trim());
        	data = LocalDate.parse(choiceCombo.getSelectedItem().toString().split(" ")[5].trim(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            ora = LocalTime.parse(choiceCombo.getSelectedItem().toString().split(" ")[7].trim(), DateTimeFormatter.ofPattern("HH:mm"));
            int res;
        	try {
				res = Admin.removePrestazione(codicefiscale, tesserino, data, ora);
				if (res == 0) {
                	JOptionPane.showMessageDialog(this,"Nessuna prestazione trovata con questi parametri",
        					"Errore",JOptionPane.ERROR_MESSAGE);
				}
				else {
			       	JOptionPane.showMessageDialog(frame, "Elemento rimosso correttamente");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
        	
 
            frame.dispose();
        });
        canvas3.add(confirm);
        frame.setVisible(true);
        frame.pack();
    }
}
