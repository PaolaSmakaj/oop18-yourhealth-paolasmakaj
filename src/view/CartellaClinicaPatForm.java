package view;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.Optional;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;

import controller.Account;
import model.CartellaClinica;
import model.Diagnosi;

public class CartellaClinicaPatForm extends JFrame {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = -8832860500438283429L;
	private GUI fac = new GUIFactory();
    private final Float font = 20.0f;
    private Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    private int width = (int) screenSize.getWidth();
   
    public CartellaClinicaPatForm(String cf) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        frame.setPreferredSize(new Dimension(width / 3, 200));
        frame.setLocationByPlatform(true);
        frame.setLayout(new BorderLayout());
        frame.setTitle("Cartella clinica di "+cf);
        frame.setResizable(false);

        JPanel canvas = fac.createBoxPanel();
        frame.add(canvas, BorderLayout.WEST);
        JPanel canvas2 = fac.createBoxPanel();
        frame.add(canvas2, BorderLayout.CENTER);
        
        JLabel labelBlood = fac.createLabelRight("Gruppo Sanguigno: ", font);
        canvas.add(labelBlood);
        JTextField textBlood = fac.createTextField();
		canvas2.add(textBlood);
		textBlood.setEditable(false);
		textBlood.setFocusable(false);
		
		JLabel labelHeight = fac.createLabelRight("Altezza: ", font);
        canvas.add(labelHeight);
        JTextField textHeight = fac.createTextField();
        canvas2.add(textHeight);
        textHeight.setEditable(false);
        
        JLabel labelWeight = fac.createLabelRight("Peso: ", font);
        canvas.add(labelWeight);
        JTextField textWeight = fac.createTextField();
        canvas2.add(textWeight);
        textWeight.setEditable(false);
        
        JLabel labelDiagnosis = fac.createLabelRight("Diagnosi: ", font);
        canvas.add(labelDiagnosis);
        JTextArea textDiagnosis = new JTextArea(10, 5);
        Border border = BorderFactory.createLineBorder(Color.GRAY, 1);
        textDiagnosis.setBorder(border);
        textDiagnosis.setLineWrap(true);
        JScrollPane sp = new JScrollPane(textDiagnosis);
        canvas2.add(sp);
        textDiagnosis.setEditable(false);
        
        CartellaClinica cc = Account.getCartellaClinica(cf);
        Optional<Diagnosi> d = cc.getUltimaDiagnosi();
        if(cc.getGruppoSanguigno().isPresent())
        	textBlood.setText(cc.getGruppoSanguigno().get());
        else 
        	textBlood.setText("Non ancora rilevato");
        
        if(d.isPresent()) {
        	textHeight.setText(String.valueOf(d.get().getAltezza()));
        	textWeight.setText(String.valueOf(d.get().getPeso()));
        }
        
        ArrayList<Diagnosi> storicoDiagnosi = cc.getDiagnosi();
        if(storicoDiagnosi.size() > 0) {
        	StringBuilder sb = new StringBuilder();
        	for(Diagnosi i : storicoDiagnosi) {
        		sb.append("Data: " +i.getDataDiagnosi() +"\n");
        		sb.append("Tesserino: " +i.getTesserino() +"\n");
        		sb.append("Diagnosi: " +i.getDiagnosi()+"\n");
        		sb.append("------------------------------------------\n");
        	}
        	textDiagnosis.setText(sb.toString());
        }
        /*
        ArrayList<String> oldDiagnosis = AccountPaziente.getDiagnosiVecchie(cf);
        if(oldDiagnosis != null) {
        	textDiagnosis.setText(oldDiagnosis.get(0));
        	textHeight.setText(oldDiagnosis.get(1));
        	textWeight.setText(oldDiagnosis.get(2));
        	textBlood.setText(oldDiagnosis.get(3));
        }*/
        
        frame.setVisible(true);
        frame.pack();
    }
}

