package view;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import controller.AccountDottore;

public class PazientiInCuraForm extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3296272073151915486L;
	private Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	private int width = (int) screenSize.getWidth();
	private int height = (int) screenSize.getHeight();
	private GUI fac = new GUIFactory();
	private String codicefiscale;
	private final float font = 20.0f;


	public PazientiInCuraForm(int tesserino) {
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		frame.setSize(width / 3, height / 3);
		frame.setLocationByPlatform(true);
		frame.setLayout(new BorderLayout());
		frame.setTitle("Seleziona Paziente");
		frame.setResizable(false);


		//Panels
		JPanel canvas = fac.createBoxPanel();
		frame.add(canvas, BorderLayout.WEST);
		JPanel canvas2 = fac.createBoxPanel();
		frame.add(canvas2, BorderLayout.CENTER);
		JPanel canvas3 = fac.createFlowPanel();
		frame.add(canvas3,  BorderLayout.EAST);

		JLabel labelCF = fac.createLabelRight("Paziente: ", font);
		canvas.add(labelCF);
		ArrayList<String> pats = AccountDottore.getPazientiInCura(String.valueOf(tesserino));

		JComboBox<String> textCF;

		JButton confirm = new JButton("Invio");
		if(pats != null && pats.size() != 0) {
			String[] desc = new String[pats.size()];
			pats.toArray(desc);
			
			textCF = fac.createCombo(desc);
		} else {
			if(pats == null) {
				textCF = fac.createCombo(new String[] {"Errore nell'ottenimento dei pazienti dal DB"});
			} else {
				textCF = fac.createCombo(new String[] {"Nessun paziente trovato"});
			}
			textCF.setEnabled(false);
        	confirm.setEnabled(false);
		}
		canvas2.add(textCF);
		confirm.setFont(new Font("Calibri", Font.PLAIN,18));
		confirm.setBackground(Color.darkGray);
		confirm.setForeground(Color.white);
		confirm.addActionListener(a -> {

			codicefiscale = textCF.getSelectedItem().toString().split(" - cf: ")[1];
			new CartellaClinicaForm(codicefiscale, tesserino);

			frame.dispose();
		});
		canvas3.add(confirm);
		frame.setVisible(true);
		frame.pack();
	}
}