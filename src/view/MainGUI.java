package view;

import controller.Database;

public class MainGUI {
	
	public static void main(String[] args){
		try {
			Database.createTableRuoli();
			Database.createTablePazienti();
			Database.createTableDottori();
			Database.createTableMacchinari();
			Database.createTableAmbulatori();
			Database.createTablePrestazioni();
			Database.createTableCartelleCliniche();
			Database.createTableDiagnosi();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		new LoginForm();
	}
}
