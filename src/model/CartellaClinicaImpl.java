package model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Optional;

public class CartellaClinicaImpl implements CartellaClinica {

	private String codiceFiscale;
	private Optional<String> gruppoSanguigno;
	private ArrayList<Diagnosi> diagnosi;
	
	public CartellaClinicaImpl(String codiceFiscale, Optional<String> gruppoSanguigno) {
		this.codiceFiscale = codiceFiscale;
		this.gruppoSanguigno = gruppoSanguigno;
		this.diagnosi = new ArrayList<Diagnosi>();
	}
	
	@Override
	public void aggiungiDiagnosi(Diagnosi diagnosi) {
		this.diagnosi.add(diagnosi);

	}

	@Override
	public Optional<Diagnosi> getDiagnosi(int tesserino, String data) {
		return diagnosi
				.stream()
				.filter(d -> d.getTesserino() == tesserino && d.getDataDiagnosi().equals(data))
				.findFirst();
	}
	
	public Optional<Diagnosi> getUltimaDiagnosi() {
		return diagnosi
				.stream()
				.max(Comparator.comparing(Diagnosi::getDataDiagnosi));
		
	}

	@Override
	public void rimuoviDiagnosi(int tesserino, String data) {
		Optional<Diagnosi> opt = diagnosi
				.stream()
				.filter(d -> d.getTesserino() == tesserino && d.getDataDiagnosi().equals(data))
				.findFirst();
		
		if(opt.isPresent())
			this.diagnosi.remove(opt.get());
	}

	public String getCodiceFiscale() {
		return codiceFiscale;
	}

	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}

	public Optional<String> getGruppoSanguigno() {
		return gruppoSanguigno;
	}

	public void setGruppoSanguigno(Optional<String> gruppoSanguigno) {
		this.gruppoSanguigno = gruppoSanguigno;
	}

	public ArrayList<Diagnosi> getDiagnosi() {
		return diagnosi;
	}

	public void setDiagnosi(ArrayList<Diagnosi> diagnosi) {
		this.diagnosi = diagnosi;
	}

}
