package model;

import java.util.ArrayList;
import java.util.Optional;

public interface CartellaClinica {
	
	/**
	 * 
	 * @param diagnosi
	 */
	void aggiungiDiagnosi(Diagnosi diagnosi);
	
	/**
	 * 
	 * @param tesserino
	 * @param data
	 * @return diagnosis object
	 */
	Optional<Diagnosi> getDiagnosi(int tesserino, String data);
	
	/**
	 * 
	 * @return the last diagnosis for this patient
	 */
	Optional<Diagnosi> getUltimaDiagnosi();
	
	/**
	 * 
	 * @param tesserino
	 * @param data
	 */
	void rimuoviDiagnosi(int tesserino, String data);
	
	/**
	 * 
	 * @return an optional of the blood type
	 */
	
	Optional<String> getGruppoSanguigno();
	
	/**
	 * 
	 * @return the list of all the diagnosis for this patient
	 */
	ArrayList<Diagnosi> getDiagnosi();
	
}
