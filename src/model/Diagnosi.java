package model;

public class Diagnosi {

	private int tesserino;
	private String dataDiagnosi;
	private double altezza;
	private double peso;
	private String diagnosi;
	
	public Diagnosi(int tesserino, String dataDiagnosi, double altezza, double peso, String diagnosi) {
		this.tesserino = tesserino;
		this.dataDiagnosi = dataDiagnosi;
		this.altezza = altezza;
		this.peso = peso;
		this.diagnosi = diagnosi;
	}

	public int getTesserino() {
		return tesserino;
	}

	public void setTesserino(int tesserino) {
		this.tesserino = tesserino;
	}

	public String getDataDiagnosi() {
		return dataDiagnosi;
	}

	public void setDataDiagnosi(String dataDiagnosi) {
		this.dataDiagnosi = dataDiagnosi;
	}

	public double getAltezza() {
		return altezza;
	}

	public void setAltezza(double altezza) {
		this.altezza = altezza;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public String getDiagnosi() {
		return diagnosi;
	}

	public void setDiagnosi(String diagnosi) {
		this.diagnosi = diagnosi;
	}
}
